<!doctype html>
<html lang="de" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="referrer" content="no-referrer">
  <meta name="robots" content="noindex,nofollow">
  <style id="cfs-style">html{display: none;}</style>
  <link rel="icon" href="favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="./themes/pmahomme/jquery/jquery-ui.css">
  <link rel="stylesheet" type="text/css" href="js/vendor/codemirror/lib/codemirror.css?v=5.2.0">
  <link rel="stylesheet" type="text/css" href="js/vendor/codemirror/addon/hint/show-hint.css?v=5.2.0">
  <link rel="stylesheet" type="text/css" href="js/vendor/codemirror/addon/lint/lint.css?v=5.2.0">
  <link rel="stylesheet" type="text/css" href="./themes/pmahomme/css/theme.css?v=5.2.0">
  <title>192.168.64.2 / localhost / krautundrueben | phpMyAdmin 5.2.0</title>
    <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery.min.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery-migrate.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/sprintf.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/ajax.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/keyhandler.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery-ui.min.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/name-conflict-fixes.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/bootstrap/bootstrap.bundle.min.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/js.cookie.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery.validate.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery-ui-timepicker-addon.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/jquery/jquery.debounce-1.0.6.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/menu_resizer.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/cross_framing_protection.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/messages.php?l=de&v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/config.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/doclinks.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/functions.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/navigation.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/indexes.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/common.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/page_settings.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/database/structure.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/table/change.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/lib/codemirror.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/mode/sql/sql.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/addon/runmode/runmode.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/addon/hint/show-hint.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/addon/hint/sql-hint.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/codemirror/addon/lint/lint.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/codemirror/addon/lint/sql-lint.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/vendor/tracekit.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/error_report.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/drag_drop_import.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/shortcuts_handler.js?v=5.2.0"></script>
  <script data-cfasync="false" type="text/javascript" src="js/dist/console.js?v=5.2.0"></script>

<script data-cfasync="false" type="text/javascript">
// <![CDATA[
CommonParams.setAll({common_query:"",opendb_url:"index.php?route=/database/structure",lang:"de",server:"1",table:"",db:"krautundrueben",token:"777a4c486c5068465748377176516346",text_dir:"ltr",LimitChars:"50",pftext:"P",confirm:true,LoginCookieValidity:"1440",session_gc_maxlifetime:"1440",logged_in:true,is_https:false,rootPath:"/phpmyadmin/",arg_separator:"&",version:"5.2.0",auth_type:"config",user:"root"});
var firstDayOfCalendar = '0';
var themeImagePath = '.\/themes\/pmahomme\/img\/';
var mysqlDocTemplate = '.\/url.php\u003Furl\u003Dhttps\u00253A\u00252F\u00252Fdev.mysql.com\u00252Fdoc\u00252Frefman\u00252F8.0\u00252Fen\u00252F\u002525s.html';
var maxInputVars = 1000;

if ($.datepicker) {
  $.datepicker.regional[''].closeText = 'Fertig';
  $.datepicker.regional[''].prevText = 'Vorher';
  $.datepicker.regional[''].nextText = 'N\u00E4chste';
  $.datepicker.regional[''].currentText = 'Heute';
  $.datepicker.regional[''].monthNames = [
    'Januar',
    'Februar',
    'M\u00E4rz',
    'April',
    'Mai',
    'Juni',
    'Juli',
    'August',
    'September',
    'Oktober',
    'November',
    'Dezember',
  ];
  $.datepicker.regional[''].monthNamesShort = [
    'Jan',
    'Feb',
    'Mrz',
    'Apr',
    'Mai',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Okt',
    'Nov',
    'Dez',
  ];
  $.datepicker.regional[''].dayNames = [
    'Sonntag',
    'Montag',
    'Dienstag',
    'Mittwoch',
    'Donnerstag',
    'Freitag',
    'Samstag',
  ];
  $.datepicker.regional[''].dayNamesShort = [
    'So',
    'Mo',
    'Di',
    'Mi',
    'Do',
    'Fr',
    'Sa',
  ];
  $.datepicker.regional[''].dayNamesMin = [
    'So',
    'Mo',
    'Di',
    'Mi',
    'Do',
    'Fr',
    'Sa',
  ];
  $.datepicker.regional[''].weekHeader = 'Wo';
  $.datepicker.regional[''].showMonthAfterYear = false;
  $.datepicker.regional[''].yearSuffix = 'keine';
  $.extend($.datepicker._defaults, $.datepicker.regional['']);
}

if ($.timepicker) {
  $.timepicker.regional[''].timeText = 'Zeit';
  $.timepicker.regional[''].hourText = 'Stunde';
  $.timepicker.regional[''].minuteText = 'Minute';
  $.timepicker.regional[''].secondText = 'Sekunde';
  $.extend($.timepicker._defaults, $.timepicker.regional['']);
}

function extendingValidatorMessages () {
  $.extend($.validator.messages, {
    required: 'Dieses\u0020Feld\u0020muss\u0020ausgef\u00FCllt\u0020werden',
    remote: 'Bitte\u0020korrigieren\u0020Sie\u0020dieses\u0020Feld',
    email: 'Bitte\u0020geben\u0020Sie\u0020eine\u0020g\u00FCltigen\u0020E\u002DMail\u002DAdresse\u0020ein',
    url: 'Bitte\u0020geben\u0020Sie\u0020eine\u0020g\u00FCltige\u0020URL\u0020ein',
    date: 'Bitte\u0020geben\u0020Sie\u0020ein\u0020g\u00FCltiges\u0020Datum\u0020ein',
    dateISO: 'Bitte\u0020geben\u0020Sie\u0020ein\u0020g\u00FCltiges\u0020Datum\u0020\u0028ISO\u0029\u0020ein',
    number: 'Bitte\u0020geben\u0020Sie\u0020eine\u0020g\u00FCltige\u0020Zahl\u0020ein',
    creditcard: 'Bitte\u0020geben\u0020Sie\u0020eine\u0020g\u00FCltige\u0020Kreditkartennummer\u0020ein',
    digits: 'Bitte\u0020geben\u0020Sie\u0020nur\u0020Ziffern\u0020ein',
    equalTo: 'Bitte\u0020geben\u0020Sie\u0020denselben\u0020Wert\u0020nochmal\u0020ein',
    maxlength: $.validator.format('Bitte\u0020nicht\u0020mehr\u0020als\u0020\u007B0\u007D\u0020Zeichen\u0020eingeben'),
    minlength: $.validator.format('Bitte\u0020geben\u0020Sie\u0020mindestens\u0020\u007B0\u007D\u0020Zeichen\u0020ein'),
    rangelength: $.validator.format('Bitte\u0020geben\u0020Sie\u0020einen\u0020zwischen\u0020\u007B0\u007D\u0020und\u0020\u007B1\u007D\u0020Zeichen\u0020langen\u0020Wert\u0020ein'),
    range: $.validator.format('Bitte\u0020geben\u0020Sie\u0020einen\u0020Wert\u0020zwischen\u0020\u007B0\u007D\u0020und\u0020\u007B1\u007D\u0020ein'),
    max: $.validator.format('Bitte\u0020geben\u0020Sie\u0020einen\u0020Wert\u0020kleiner\u0020gleich\u0020\u007B0\u007D\u0020ein'),
    min: $.validator.format('Bitte\u0020geben\u0020Sie\u0020einen\u0020Wert\u0020gr\u00F6\u00DFer\u0020gleich\u0020\u007B0\u007D\u0020ein'),
    validationFunctionForDateTime: $.validator.format('Bitte\u0020geben\u0020Sie\u0020ein\u0020g\u00FCltiges\u0020Datum\u0020oder\u0020eine\u0020g\u00FCltige\u0020Zeitangabe\u0020ein'),
    validationFunctionForHex: $.validator.format('Bitte\u0020geben\u0020Sie\u0020einen\u0020g\u00FCltige\u0020hexadezimale\u0020Eingabe\u0020ein'),
    validationFunctionForMd5: $.validator.format('Diese\u0020Spalte\u0020darf\u0020keinen\u002032\u002DZeichen\u002DWert\u0020enthalten'),
    validationFunctionForAesDesEncrypt: $.validator.format('Diese\u0020Funktionen\u0020sollen\u0020ein\u0020bin\u00E4res\u0020Ergebnis\u0020zur\u00FCckgeben\u003B\u0020Um\u0020inkonsistente\u0020Ergebnisse\u0020zu\u0020vermeiden,\u0020sollten\u0020Sie\u0020sie\u0020in\u0020einer\u0020BINARY\u002D,\u0020VARBINARY\u002D\u0020oder\u0020BLOB\u002DSpalte\u0020speichern.')
  });
}

ConsoleEnterExecutes=false

AJAX.scriptHandler
  .add('vendor/jquery/jquery.min.js', 0)
  .add('vendor/jquery/jquery-migrate.js', 0)
  .add('vendor/sprintf.js', 1)
  .add('ajax.js', 0)
  .add('keyhandler.js', 1)
  .add('vendor/jquery/jquery-ui.min.js', 0)
  .add('name-conflict-fixes.js', 1)
  .add('vendor/bootstrap/bootstrap.bundle.min.js', 1)
  .add('vendor/js.cookie.js', 1)
  .add('vendor/jquery/jquery.validate.js', 0)
  .add('vendor/jquery/jquery-ui-timepicker-addon.js', 0)
  .add('vendor/jquery/jquery.debounce-1.0.6.js', 0)
  .add('menu_resizer.js', 1)
  .add('cross_framing_protection.js', 0)
  .add('messages.php', 0)
  .add('config.js', 1)
  .add('doclinks.js', 1)
  .add('functions.js', 1)
  .add('navigation.js', 1)
  .add('indexes.js', 1)
  .add('common.js', 1)
  .add('page_settings.js', 1)
  .add('database/structure.js', 1)
  .add('table/change.js', 1)
  .add('vendor/codemirror/lib/codemirror.js', 0)
  .add('vendor/codemirror/mode/sql/sql.js', 0)
  .add('vendor/codemirror/addon/runmode/runmode.js', 0)
  .add('vendor/codemirror/addon/hint/show-hint.js', 0)
  .add('vendor/codemirror/addon/hint/sql-hint.js', 0)
  .add('vendor/codemirror/addon/lint/lint.js', 0)
  .add('codemirror/addon/lint/sql-lint.js', 0)
  .add('vendor/tracekit.js', 1)
  .add('error_report.js', 1)
  .add('drag_drop_import.js', 1)
  .add('shortcuts_handler.js', 1)
  .add('console.js', 1)
;
$(function() {
        AJAX.fireOnload('vendor/sprintf.js');
        AJAX.fireOnload('keyhandler.js');
        AJAX.fireOnload('name-conflict-fixes.js');
      AJAX.fireOnload('vendor/bootstrap/bootstrap.bundle.min.js');
      AJAX.fireOnload('vendor/js.cookie.js');
            AJAX.fireOnload('menu_resizer.js');
          AJAX.fireOnload('config.js');
      AJAX.fireOnload('doclinks.js');
      AJAX.fireOnload('functions.js');
      AJAX.fireOnload('navigation.js');
      AJAX.fireOnload('indexes.js');
      AJAX.fireOnload('common.js');
      AJAX.fireOnload('page_settings.js');
      AJAX.fireOnload('database/structure.js');
      AJAX.fireOnload('table/change.js');
                    AJAX.fireOnload('vendor/tracekit.js');
      AJAX.fireOnload('error_report.js');
      AJAX.fireOnload('drag_drop_import.js');
      AJAX.fireOnload('shortcuts_handler.js');
      AJAX.fireOnload('console.js');
  });
// ]]>
</script>

  <noscript><style>html{display:block}</style></noscript>
</head>
<body>
    <div id="pma_navigation" class="d-print-none" data-config-navigation-width="240">
    <div id="pma_navigation_resizer"></div>
    <div id="pma_navigation_collapser"></div>
    <div id="pma_navigation_content">
      <div id="pma_navigation_header">

                  <div id="pmalogo">
                          <a href="index.php">
                                      <img id="imgpmalogo" src="./themes/pmahomme/img/logo_left.png" alt="phpMyAdmin">
                                      </a>
                      </div>
        
        <div id="navipanellinks">
          <a href="index.php?route=/" title="Startseite"><img src="themes/dot.gif" title="Startseite" alt="Startseite" class="icon ic_b_home"></a>

                      <a class="logout disableAjax" href="index.php?route=/logout" title="Leere Sitzungsdaten"><img src="themes/dot.gif" title="Leere Sitzungsdaten" alt="Leere Sitzungsdaten" class="icon ic_s_loggoff"></a>
          
          <a href="./doc/html/index.html" title="phpMyAdmin-Dokumentation" target="_blank" rel="noopener noreferrer"><img src="themes/dot.gif" title="phpMyAdmin-Dokumentation" alt="phpMyAdmin-Dokumentation" class="icon ic_b_docs"></a>

          <a href="./url.php?url=https%3A%2F%2Fmariadb.com%2Fkb%2Fen%2Fdocumentation%2F" title="MariaDB-Dokumentation" target="_blank" rel="noopener noreferrer"><img src="themes/dot.gif" title="MariaDB-Dokumentation" alt="MariaDB-Dokumentation" class="icon ic_b_sqlhelp"></a>

          <a id="pma_navigation_settings_icon" href="#" title="Navigationspanel-Einstellungen"><img src="themes/dot.gif" title="Navigationspanel-Einstellungen" alt="Navigationspanel-Einstellungen" class="icon ic_s_cog"></a>

          <a id="pma_navigation_reload" href="#" title="Navigations-Panel aktualisieren"><img src="themes/dot.gif" title="Navigations-Panel aktualisieren" alt="Navigations-Panel aktualisieren" class="icon ic_s_reload"></a>
        </div>

        
        <img src="themes/dot.gif" title="Laden…" alt="Laden…" style="visibility: hidden; display:none" class="icon ic_ajax_clock_small throbber">
      </div>
      <div id="pma_navigation_tree" class="list_container synced highlight autoexpand">

  <div class="pma_quick_warp">
    <div class="drop_list"><button title="Letzte Tabellen" class="drop_button btn">Letzte</button><ul id="pma_recent_list"><li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=ZUTAT">
    `krautundrueben`.`ZUTAT`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=KATEGORIE">
    `krautundrueben`.`KATEGORIE`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=BOX">
    `krautundrueben`.`BOX`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=BESTELLUNGZUTAT">
    `krautundrueben`.`BESTELLUNGZUTAT`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=ALLERGENE">
    `krautundrueben`.`ALLERGENE`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=LIEFERANT">
    `krautundrueben`.`LIEFERANT`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=Kategorie">
    `krautundrueben`.`Kategorie`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=KUNDE">
    `krautundrueben`.`KUNDE`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=krautundrueben&table=BESTELLUNG">
    `krautundrueben`.`BESTELLUNG`
  </a>
</li>
<li class="warp_link">
  <a href="index.php?route=/table/recent-favorite&db=GymAbo&table=kurse">
    `GymAbo`.`kurse`
  </a>
</li>
</ul></div>    <div class="drop_list"><button title="Favoriten-Tabellen" class="drop_button btn">Favoriten</button><ul id="pma_favorite_list"><li class="warp_link">
            Es gibt keine Favoriten-Tabellen.    </li>
</ul></div>    <div class="clearfloat"></div>
</div>


<div class="clearfloat"></div>

<ul>
  
  <!-- CONTROLS START -->
<li id="navigation_controls_outer">
    <div id="navigation_controls">
        <a href="#" id="pma_navigation_collapse" title="Alles auf-/zuklappen"><img src="themes/dot.gif" title="Alles auf-/zuklappen" alt="Alles auf-/zuklappen" class="icon ic_s_collapseall"></a>
        <a href="#" id="pma_navigation_sync" title="Verknüpfung mit Hauptpanel aufheben"><img src="themes/dot.gif" title="Verknüpfung mit Hauptpanel aufheben" alt="Verknüpfung mit Hauptpanel aufheben" class="icon ic_s_link"></a>
    </div>
</li>
<!-- CONTROLS ENDS -->

</ul>



<div id='pma_navigation_tree_content'>
  <ul>
      <li class="first new_database italics">
    <div class="block">
      <i class="first"></i>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/server/databases"><img src="themes/dot.gif" title="Neu" alt="Neu" class="icon ic_b_newdb"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/server/databases" title="Neu">Neu</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.RHVtYg==" data-vpath="cm9vdA==.RHVtYg==" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=Dumb"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=Dumb" title="Struktur">Dumb</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.R3ltQWJv" data-vpath="cm9vdA==.R3ltQWJv" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=GymAbo"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=GymAbo" title="Struktur">GymAbo</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.aW5mb3JtYXRpb25fc2NoZW1h" data-vpath="cm9vdA==.aW5mb3JtYXRpb25fc2NoZW1h" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=information_schema"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=information_schema" title="Struktur">information_schema</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.a3JhdXR1bmRydWViZW4=" data-vpath="cm9vdA==.a3JhdXR1bmRydWViZW4=" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=krautundrueben"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=krautundrueben" title="Struktur">krautundrueben</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.bXlzcWw=" data-vpath="cm9vdA==.bXlzcWw=" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=mysql"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=mysql" title="Struktur">mysql</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.cGVyZm9ybWFuY2Vfc2NoZW1h" data-vpath="cm9vdA==.cGVyZm9ybWFuY2Vfc2NoZW1h" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=performance_schema"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=performance_schema" title="Struktur">performance_schema</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="database">
    <div class="block">
      <i></i>
              <b></b>
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.cGhwbXlhZG1pbg==" data-vpath="cm9vdA==.cGhwbXlhZG1pbg==" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=phpmyadmin"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=phpmyadmin" title="Struktur">phpmyadmin</a>
          
    

    
    <div class="clearfloat"></div>



  </li>
  <li class="last database">
    <div class="block">
      <i></i>
              
        <a class="expander" href="#">
          <span class="hide paths_nav" data-apath="cm9vdA==.dGVzdA==" data-vpath="cm9vdA==.dGVzdA==" data-pos="0"></span>
                    <img src="themes/dot.gif" title="Auf-/Zuklappen" alt="Auf-/Zuklappen" class="icon ic_b_plus">
        </a>
          </div>
    
          <div class="block second">
                  <a href="index.php?route=/database/operations&db=test"><img src="themes/dot.gif" title="Datenbank-Operationen" alt="Datenbank-Operationen" class="icon ic_s_db"></a>
              </div>

              <a class="hover_show_full" href="index.php?route=/database/structure&db=test" title="Struktur">test</a>
          
    

    
    <div class="clearfloat"></div>



  </li>

  </ul>
</div>


      </div>

      <div id="pma_navi_settings_container">
                  <div id="pma_navigation_settings"><div class="page_settings"><form method="post" action="index.php&#x3F;route&#x3D;&#x25;2Fdatabase&#x25;2Fstructure&amp;db&#x3D;krautundrueben&amp;server&#x3D;1" class="config-form disableAjax">
  <input type="hidden" name="tab_hash" value="">
      <input type="hidden" name="check_page_refresh" id="check_page_refresh" value="">
    <input type="hidden" name="token" value="777a4c486c5068465748377176516346">
  <input type="hidden" name="submit_save" value="Navi">

  <ul class="nav nav-tabs" id="configFormDisplayTab" role="tablist">
          <li class="nav-item" role="presentation">
        <a class="nav-link active" id="Navi_panel-tab" href="#Navi_panel" data-bs-toggle="tab" role="tab" aria-controls="Navi_panel" aria-selected="true">Navigationspanel</a>
      </li>
          <li class="nav-item" role="presentation">
        <a class="nav-link" id="Navi_tree-tab" href="#Navi_tree" data-bs-toggle="tab" role="tab" aria-controls="Navi_tree" aria-selected="false">Navigationsbaum</a>
      </li>
          <li class="nav-item" role="presentation">
        <a class="nav-link" id="Navi_servers-tab" href="#Navi_servers" data-bs-toggle="tab" role="tab" aria-controls="Navi_servers" aria-selected="false">Server</a>
      </li>
          <li class="nav-item" role="presentation">
        <a class="nav-link" id="Navi_databases-tab" href="#Navi_databases" data-bs-toggle="tab" role="tab" aria-controls="Navi_databases" aria-selected="false">Datenbanken</a>
      </li>
          <li class="nav-item" role="presentation">
        <a class="nav-link" id="Navi_tables-tab" href="#Navi_tables" data-bs-toggle="tab" role="tab" aria-controls="Navi_tables" aria-selected="false">Tabellen</a>
      </li>
      </ul>
  <div class="tab-content">
          <div class="tab-pane fade show active" id="Navi_panel" role="tabpanel" aria-labelledby="Navi_panel-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Navigationspanel</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Aussehen des Navigationspanels anpassen.</h6>
            
            <fieldset class="optbox">
              <legend>Navigationspanel</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="ShowDatabasesNavigationAsTree">Datenbanknavigation als Baum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDatabasesNavigationAsTree" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Im Navigationsbereich den Datenbankbaum durch ein Auswahlmenü ersetzen</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDatabasesNavigationAsTree" id="ShowDatabasesNavigationAsTree" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDatabasesNavigationAsTree" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationLinkWithMainPanel">Mit Hauptpanel verknüpfen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationLinkWithMainPanel" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Verknüpfen Sie mit Hauptbereich durch die Hervorhebung der aktuellen Datenbank oder Tabelle.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationLinkWithMainPanel" id="NavigationLinkWithMainPanel" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationLinkWithMainPanel" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationDisplayLogo">Logo anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationDisplayLogo" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Logo im Navigationspanel anzeigen.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationDisplayLogo" id="NavigationDisplayLogo" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationDisplayLogo" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationLogoLink">URL für Logo-Link</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationLogoLink" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>URL, auf die das Logo im Navigationspanel zeigt.</small>
      </th>

  <td>
          <input type="text" name="NavigationLogoLink" id="NavigationLogoLink" value="index.php" class="w-75">
    
    
    
          <a class="restore-default hide" href="#NavigationLogoLink" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationLogoLinkWindow">Ziel für Logo-Link</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationLogoLinkWindow" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Öffne Links im aktuellen Fenster(<code>main</code>) oder in einem neuen Fenster (<code>new</code>).</small>
      </th>

  <td>
          <select name="NavigationLogoLinkWindow" id="NavigationLogoLinkWindow" class="w-75">
                            <option value="main" selected>main</option>
                            <option value="new">new</option>
              </select>
    
    
    
          <a class="restore-default hide" href="#NavigationLogoLinkWindow" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreePointerEnable">Hervorhebung ermöglichen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreePointerEnable" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Server unter Mauscursor hervorheben.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreePointerEnable" id="NavigationTreePointerEnable" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreePointerEnable" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="FirstLevelNavigationItems">Maximale Elemente auf erster Ebene</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_FirstLevelNavigationItems" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Anzahl der Elemente, die pro Seite auf der ersten Ebene im Navigationsbaum angezeigt werden können.</small>
      </th>

  <td>
          <input type="number" name="FirstLevelNavigationItems" id="FirstLevelNavigationItems" value="100" class="">
    
    
    
          <a class="restore-default hide" href="#FirstLevelNavigationItems" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeDisplayItemFilterMinimum">Minimale Anzahl Elemente, ab denen Filter-Feld angezeigt wird</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeDisplayItemFilterMinimum" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Legt die minimale Anzahl Elemente fest (Tabellen, Ansichten, Routinen und Ereignisse), ab denen ein Filter-Feld angezeigt wird.</small>
      </th>

  <td>
          <input type="number" name="NavigationTreeDisplayItemFilterMinimum" id="NavigationTreeDisplayItemFilterMinimum" value="30" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationTreeDisplayItemFilterMinimum" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NumRecentTables">Kürzlich verwendete Tabellen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NumRecentTables" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Maximale Anzahl der kürzlichen verwendeten Tabellen, auf 0 setzen zum deaktivieren.</small>
      </th>

  <td>
          <input type="number" name="NumRecentTables" id="NumRecentTables" value="10" class="">
    
    
    
          <a class="restore-default hide" href="#NumRecentTables" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NumFavoriteTables">Favoriten-Tabellen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NumFavoriteTables" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Maximale Anzahl der bevorzugten Tabellen, auf 0 setzen zum deaktivieren.</small>
      </th>

  <td>
          <input type="number" name="NumFavoriteTables" id="NumFavoriteTables" value="10" class="">
    
    
    
          <a class="restore-default hide" href="#NumFavoriteTables" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationWidth">Navigationspanelbreite</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationWidth" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Auf 0 setzen, um das Navigations-Panel zu verstecken.</small>
      </th>

  <td>
          <input type="number" name="NavigationWidth" id="NavigationWidth" value="240" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationWidth" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
          <div class="tab-pane fade" id="Navi_tree" role="tabpanel" aria-labelledby="Navi_tree-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Navigationsbaum</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Navigationsbaum anpassen.</h6>
            
            <fieldset class="optbox">
              <legend>Navigationsbaum</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="MaxNavigationItems">Maximale Elemente in Zweig</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_MaxNavigationItems" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Anzahl der Elemente, die pro Seite im Navigationsbaum angezeigt werden können.</small>
      </th>

  <td>
          <input type="number" name="MaxNavigationItems" id="MaxNavigationItems" value="50" class="">
    
    
    
          <a class="restore-default hide" href="#MaxNavigationItems" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeEnableGrouping">Elemente im Baum gruppieren</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeEnableGrouping" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Elemente im Navigationsbaum gruppieren (wie vom im obigen „Datenbanken und Tabellen“-Reiter definierten Trennzeichen bestimmt).</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeEnableGrouping" id="NavigationTreeEnableGrouping" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeEnableGrouping" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeEnableExpansion">Erweiterung des Navigationsbaumes aktivieren</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeEnableExpansion" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Wird die Möglichkeit der Baumerweiterung im Navigationsbereich angeboten oder nicht.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeEnableExpansion" id="NavigationTreeEnableExpansion" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeEnableExpansion" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeShowTables">Tabellen-Liste im Baum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeShowTables" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob die Tabellen unterhalb der Datenbanken im Navigationsbaum angezeigt werden</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeShowTables" id="NavigationTreeShowTables" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeShowTables" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeShowViews">Views im Navigationsbaum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeShowViews" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob Views unterhalb der Datenbanken im Navigationsbaum angezeigt werden</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeShowViews" id="NavigationTreeShowViews" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeShowViews" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeShowFunctions">Funktionsfelder im Navigationsbaum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeShowFunctions" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob Funktionsfelder unterhalb der Datenbanken im Navigationsbaum angezeigt werden</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeShowFunctions" id="NavigationTreeShowFunctions" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeShowFunctions" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeShowProcedures">Prozeduren im Navigationsbaum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeShowProcedures" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob Prozeduren unterhalb der Datenbanken im Navigationsbaum angezeigt werden</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeShowProcedures" id="NavigationTreeShowProcedures" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeShowProcedures" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeShowEvents">Events im Navigationsbaum anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeShowEvents" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob Events unterhalb der Datenbanken im Navigationsbaum angezeigt werden</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeShowEvents" id="NavigationTreeShowEvents" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeShowEvents" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeAutoexpandSingleDb">Eine einzelne Datenbank erweitern</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeAutoexpandSingleDb" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Ob eine einzelne Datenbank im Navigationsbaum automatisch erweitert werden soll.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationTreeAutoexpandSingleDb" id="NavigationTreeAutoexpandSingleDb" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeAutoexpandSingleDb" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
          <div class="tab-pane fade" id="Navi_servers" role="tabpanel" aria-labelledby="Navi_servers-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Server</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Anzeigeoptionen für Server.</h6>
            
            <fieldset class="optbox">
              <legend>Server</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="NavigationDisplayServers">Server-Auswahl anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationDisplayServers" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Serverauswahl oben im Navigationspanel anzeigen.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="NavigationDisplayServers" id="NavigationDisplayServers" checked>
      </span>
    
    
    
          <a class="restore-default hide" href="#NavigationDisplayServers" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="DisplayServersList">Server als Liste anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_DisplayServersList" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Server als Liste statt Dropdownfeld anzeigen.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="DisplayServersList" id="DisplayServersList">
      </span>
    
    
    
          <a class="restore-default hide" href="#DisplayServersList" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
          <div class="tab-pane fade" id="Navi_databases" role="tabpanel" aria-labelledby="Navi_databases-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Datenbanken</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Anzeigeoptionen für Datenbank.</h6>
            
            <fieldset class="optbox">
              <legend>Datenbanken</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="NavigationTreeDisplayDbFilterMinimum">Minimale Anzahl der Datenbanken, die im Datenbank-Filterfeld angezeigt werden</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeDisplayDbFilterMinimum" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
      </th>

  <td>
          <input type="number" name="NavigationTreeDisplayDbFilterMinimum" id="NavigationTreeDisplayDbFilterMinimum" value="30" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationTreeDisplayDbFilterMinimum" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeDbSeparator">Trennzeichen für Datenbank-Baum</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeDbSeparator" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Zeichenkette, die Datenbanken in unterschiedliche Baum-Abschnitte unterteilt.</small>
      </th>

  <td>
                <input type="text" size="25" name="NavigationTreeDbSeparator" id="NavigationTreeDbSeparator" value="_" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationTreeDbSeparator" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
          <div class="tab-pane fade" id="Navi_tables" role="tabpanel" aria-labelledby="Navi_tables-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Tabellen</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Anzeigeoptionen für Tabellen.</h6>
            
            <fieldset class="optbox">
              <legend>Tabellen</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="NavigationTreeDefaultTabTable">Ziel für Schnellzugriff-Icon</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeDefaultTabTable" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
      </th>

  <td>
          <select name="NavigationTreeDefaultTabTable" id="NavigationTreeDefaultTabTable" class="w-75">
                            <option value="structure" selected>Struktur</option>
                            <option value="sql">SQL</option>
                            <option value="search">Suche</option>
                            <option value="insert">Einfügen</option>
                            <option value="browse">Anzeigen</option>
              </select>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeDefaultTabTable" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeDefaultTabTable2">Ziel für zweites Schnellzugriff-Icon</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeDefaultTabTable2" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
      </th>

  <td>
          <select name="NavigationTreeDefaultTabTable2" id="NavigationTreeDefaultTabTable2" class="w-75">
                            <option value="" selected></option>
                            <option value="structure">Struktur</option>
                            <option value="sql">SQL</option>
                            <option value="search">Suche</option>
                            <option value="insert">Einfügen</option>
                            <option value="browse">Anzeigen</option>
              </select>
    
    
    
          <a class="restore-default hide" href="#NavigationTreeDefaultTabTable2" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeTableSeparator">Trennzeichen für Tabellen-Baum</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeTableSeparator" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Zeichenkette, die Tabellen in unterschiedliche Baum-Abschnitte unterteilt.</small>
      </th>

  <td>
                <input type="text" size="25" name="NavigationTreeTableSeparator" id="NavigationTreeTableSeparator" value="__" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationTreeTableSeparator" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="NavigationTreeTableLevel">Maximale Tiefe des Tabellen-Baumes</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_NavigationTreeTableLevel" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
      </th>

  <td>
          <input type="number" name="NavigationTreeTableLevel" id="NavigationTreeTableLevel" value="1" class="">
    
    
    
          <a class="restore-default hide" href="#NavigationTreeTableLevel" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
      </div>
</form>

<script type="text/javascript">
  if (typeof configInlineParams === 'undefined' || !Array.isArray(configInlineParams)) {
    configInlineParams = [];
  }
  configInlineParams.push(function () {
    registerFieldValidator('FirstLevelNavigationItems', 'validatePositiveNumber', true);
registerFieldValidator('NavigationTreeDisplayItemFilterMinimum', 'validatePositiveNumber', true);
registerFieldValidator('NumRecentTables', 'validateNonNegativeNumber', true);
registerFieldValidator('NumFavoriteTables', 'validateNonNegativeNumber', true);
registerFieldValidator('NavigationWidth', 'validateNonNegativeNumber', true);
registerFieldValidator('MaxNavigationItems', 'validatePositiveNumber', true);
registerFieldValidator('NavigationTreeTableLevel', 'validatePositiveNumber', true);

    $.extend(Messages, {
      'error_nan_p': 'Keine\u0020positive\u0020Zahl\u0021',
      'error_nan_nneg': 'Keine\u0020nicht\u002Dnegative\u0020Zahl\u0021',
      'error_incorrect_port': 'Keine\u0020g\u00FCltige\u0020Portnummer\u0021',
      'error_invalid_value': 'Ung\u00FCltiger\u0020Wert\u0021',
      'error_value_lte': 'Wert\u0020muss\u0020gleich\u0020oder\u0020kleiner\u0020als\u0020\u0025s\u0020sein\u0021',
    });

    $.extend(defaultValues, {
      'ShowDatabasesNavigationAsTree': true,
      'NavigationLinkWithMainPanel': true,
      'NavigationDisplayLogo': true,
      'NavigationLogoLink': 'index.php',
      'NavigationLogoLinkWindow': ['main'],
      'NavigationTreePointerEnable': true,
      'FirstLevelNavigationItems': '100',
      'NavigationTreeDisplayItemFilterMinimum': '30',
      'NumRecentTables': '10',
      'NumFavoriteTables': '10',
      'NavigationWidth': '240',
      'MaxNavigationItems': '50',
      'NavigationTreeEnableGrouping': true,
      'NavigationTreeEnableExpansion': true,
      'NavigationTreeShowTables': true,
      'NavigationTreeShowViews': true,
      'NavigationTreeShowFunctions': true,
      'NavigationTreeShowProcedures': true,
      'NavigationTreeShowEvents': true,
      'NavigationTreeAutoexpandSingleDb': true,
      'NavigationDisplayServers': true,
      'DisplayServersList': false,
      'NavigationTreeDisplayDbFilterMinimum': '30',
      'NavigationTreeDbSeparator': '_',
      'NavigationTreeDefaultTabTable': ['structure'],
      'NavigationTreeDefaultTabTable2': [''],
      'NavigationTreeTableSeparator': '__',
      'NavigationTreeTableLevel': '1'
    });
  });
  if (typeof configScriptLoaded !== 'undefined' && configInlineParams) {
    loadInlineConfig();
  }
</script>
</div></div>
              </div>
    </div>

          <div class="pma_drop_handler">
        Dateien hier ablegen      </div>
      <div class="pma_sql_import_status">
        <h2>
          SQL-Upload          ( <span class="pma_import_count">0</span> )
          <span class="close">x</span>
          <span class="minimize">-</span>
        </h2>
        <div></div>
      </div>
      </div>
  <div class="modal fade" id="unhideNavItemModal" tabindex="-1" aria-labelledby="unhideNavItemModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="unhideNavItemModalLabel">Zeige versteckte Navigationselemente.</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schließen"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>


  
  

  
      <noscript>
      <div class="alert alert-danger" role="alert">
  <img src="themes/dot.gif" title="" alt="" class="icon ic_s_error"> Javascript muss ab hier aktiviert sein!
</div>

    </noscript>
  
      <div id="floating_menubar" class="d-print-none"></div>
<nav id="server-breadcrumb" aria-label="breadcrumb">
  <ol class="breadcrumb breadcrumb-navbar">
    <li class="breadcrumb-item">
      <img src="themes/dot.gif" title="" alt="" class="icon ic_s_host">
      <a href="index.php?route=/" data-raw-text="localhost" draggable="false">
        Server:        localhost
      </a>
    </li>

          <li class="breadcrumb-item">
        <img src="themes/dot.gif" title="" alt="" class="icon ic_s_db">
        <a href="index.php?route=/database/structure&db=krautundrueben" data-raw-text="krautundrueben" draggable="false">
          Datenbank:          krautundrueben
        </a>
      </li>

            </ol>
</nav>
<div id="topmenucontainer" class="menucontainer">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-label="Navigation umschalten" aria-controls="navbarNav" aria-expanded="false">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul id="topmenu" class="navbar-nav">
                  <li class="nav-item active">
            <a class="nav-link text-nowrap" href="index.php?route=/database/structure&db=krautundrueben">
              <img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur
                              <span class="visually-hidden">(aktuelle)</span>
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/sql&db=krautundrueben">
              <img src="themes/dot.gif" title="SQL" alt="SQL" class="icon ic_b_sql">&nbsp;SQL
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/search&db=krautundrueben">
              <img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_search">&nbsp;Suche
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/multi-table-query&db=krautundrueben">
              <img src="themes/dot.gif" title="Abfrage" alt="Abfrage" class="icon ic_s_db">&nbsp;Abfrage
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/export&db=krautundrueben">
              <img src="themes/dot.gif" title="Exportieren" alt="Exportieren" class="icon ic_b_export">&nbsp;Exportieren
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/import&db=krautundrueben">
              <img src="themes/dot.gif" title="Importieren" alt="Importieren" class="icon ic_b_import">&nbsp;Importieren
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/operations&db=krautundrueben">
              <img src="themes/dot.gif" title="Operationen" alt="Operationen" class="icon ic_b_tblops">&nbsp;Operationen
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/server/privileges&db=krautundrueben&checkprivsdb=krautundrueben&viewing_mode=db">
              <img src="themes/dot.gif" title="Rechte" alt="Rechte" class="icon ic_s_rights">&nbsp;Rechte
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/routines&db=krautundrueben">
              <img src="themes/dot.gif" title="Routinen" alt="Routinen" class="icon ic_b_routines">&nbsp;Routinen
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/events&db=krautundrueben">
              <img src="themes/dot.gif" title="Ereignisse" alt="Ereignisse" class="icon ic_b_events">&nbsp;Ereignisse
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/triggers&db=krautundrueben">
              <img src="themes/dot.gif" title="Trigger" alt="Trigger" class="icon ic_b_triggers">&nbsp;Trigger
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/tracking&db=krautundrueben">
              <img src="themes/dot.gif" title="Nachverfolgung" alt="Nachverfolgung" class="icon ic_eye">&nbsp;Nachverfolgung
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/designer&db=krautundrueben">
              <img src="themes/dot.gif" title="Designer" alt="Designer" class="icon ic_b_relations">&nbsp;Designer
                          </a>
          </li>
                  <li class="nav-item">
            <a class="nav-link text-nowrap" href="index.php?route=/database/central-columns&db=krautundrueben">
              <img src="themes/dot.gif" title="Zentrale Spalten" alt="Zentrale Spalten" class="icon ic_centralColumns">&nbsp;Zentrale Spalten
                          </a>
          </li>
              </ul>
    </div>
  </nav>
</div>

    <span id="page_nav_icons" class="d-print-none">
      <span id="lock_page_icon"></span>
      <span id="page_settings_icon">
        <img src="themes/dot.gif" title="Seitenbezogene Einstellungen" alt="Seitenbezogene Einstellungen" class="icon ic_s_cog">
      </span>
      <a id="goto_pagetop" href="#"><img src="themes/dot.gif" title="Klicken Sie auf die Leiste, um zum Anfang der Seite zu scrollen" alt="Klicken Sie auf die Leiste, um zum Anfang der Seite zu scrollen" class="icon ic_s_top"></a>
    </span>
  
  <div id="pma_console_container" class="d-print-none">
    <div id="pma_console">
                <div class="toolbar collapsed">
                    <div class="switch_button console_switch">
            <img src="themes/dot.gif" title="SQL-Abfragekonsole" alt="SQL-Abfragekonsole" class="icon ic_console">
            <span>Konsole</span>
        </div>
                            <div class="button clear">
            
            <span>Werte löschen</span>
        </div>
                            <div class="button history">
            
            <span>Verlaufsprotokoll</span>
        </div>
                            <div class="button options">
            
            <span>Optionen</span>
        </div>
                            <div class="button bookmarks">
            
            <span>Lesezeichen</span>
        </div>
                            <div class="button debug hide">
            
            <span>SQL Debugger</span>
        </div>
            </div>
                <div class="content">
            <div class="console_message_container">
                <div class="message welcome">
                    <span id="instructions-0">
                        Drücken Sie Strg+Enter, um die Abfrage auszuführen                    </span>
                    <span class="hide" id="instructions-1">
                        Drücken Sie Enter, um die Abfrage auszuführen                    </span>
                </div>
                                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="ZUTAT">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `ZUTAT`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">CREATE TABLE Kategorie (
    Bezeichnung varchar(25)
    
    
    
    
    
    
    
    
    
    
    
    );</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="Kategorie">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `Kategorie`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="Kategorie">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `Kategorie`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="Kategorie">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">DROP TABLE Kategorie;</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">CREATE TABLE KATEGORIE(
	Bezeichnung varchar(25) PRIMARY KEY   
);</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `KATEGORIE`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">INSERT INTO KATEGORIE (Bezeichnung) VALUES (&#039;Vegan&#039;),(&#039;Vegetarisch&#039;),(&#039;Low Carb&#039;);</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `KATEGORIE`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `KATEGORIE`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="LIEFERANT">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `LIEFERANT`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">CREATE TABLE ALLERGENE (
    Bezeichnung varchar(25) PRIMARY KEY
    
    
    
    );</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">INSERT INTO ALLERGENE (Bezeichnung) VALUES (&#039;Laktose&#039;),(&#039;Gluten&#039;),(&#039;Nüsse&#039;);</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="ALLERGENE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `ALLERGENE`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">CREATE TABLE BOX (
    BoxID int PRIMARY KEY
                  ,Bezeichnung varchar(25)
                  ,Menge int
                 );</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BOX`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BOX`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `KATEGORIE`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BOX`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BESTELLUNGZUTAT">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BESTELLUNGZUTAT`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BOX`</span>
                        </div>
                                            <div class="message history collapsed hide"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">ALTER TABLE BOX
DROP COLUMN Menge;</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="BOX">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `BOX`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="KATEGORIE">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `KATEGORIE`</span>
                        </div>
                                            <div class="message history collapsed hide select"
                            targetdb="krautundrueben" targettable="ZUTAT">
                            <div class="action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span>krautundrueben</span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span>Während der aktuellen Sitzung</span>
                    </span>
            </div>
                            <span class="query">SELECT * FROM `ZUTAT`</span>
                        </div>
                                                </div><!-- console_message_container -->
            <div class="query_input">
                <span class="console_query_input"></span>
            </div>
        </div><!-- message end -->
                <div class="mid_layer"></div>
                <div class="card" id="debug_console">
            <div class="toolbar ">
                    <div class="button order order_asc">
            
            <span>aufsteigend</span>
        </div>
                            <div class="button order order_desc">
            
            <span>absteigend</span>
        </div>
                            <div class="text">
            
            <span>Reihenfolge:</span>
        </div>
                            <div class="switch_button">
            
            <span>SQL Debugger</span>
        </div>
                            <div class="button order_by sort_count">
            
            <span>Zähler</span>
        </div>
                            <div class="button order_by sort_exec">
            
            <span>Ausführungsreihenfolge</span>
        </div>
                            <div class="button order_by sort_time">
            
            <span>Nötige Zeit</span>
        </div>
                            <div class="text">
            
            <span>Sortieren nach:</span>
        </div>
                            <div class="button group_queries">
            
            <span>Abfragen umgruppieren</span>
        </div>
                            <div class="button ungroup_queries">
            
            <span>Abfragen voneinander lösen</span>
        </div>
            </div>
            <div class="content debug">
                <div class="message welcome"></div>
                <div class="debugLog"></div>
            </div> <!-- Content -->
            <div class="templates">
                <div class="debug_query action_content">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action dbg_show_trace">
            Nachverfolgung anzeigen
                    </span>
                            <span class="action dbg_hide_trace">
            Nachverfolgung ausblenden
                    </span>
                            <span class="text count hide">
            Zähler
                    </span>
                            <span class="text time">
            Nötige Zeit
                    </span>
            </div>
            </div> <!-- Template -->
        </div> <!-- Debug SQL card -->
                    <div class="card" id="pma_bookmarks">
                <div class="toolbar ">
                    <div class="switch_button">
            
            <span>Lesezeichen</span>
        </div>
                            <div class="button refresh">
            
            <span>Aktualisieren</span>
        </div>
                            <div class="button add">
            
            <span>Hinzufügen</span>
        </div>
            </div>
                <div class="content bookmark">
                    <div class="message welcome">
    <span>Keine Lesezeichen</span>
</div>

                </div>
                <div class="mid_layer"></div>
                <div class="card add">
                    <div class="toolbar ">
                    <div class="switch_button">
            
            <span>Lesezeichen hinzufügen</span>
        </div>
            </div>
                    <div class="content add_bookmark">
                        <div class="options">
                            <label>
                                Titel: <input type="text" name="label">
                            </label>
                            <label>
                                Ziel-Datenbank: <input type="text" name="targetdb">
                            </label>
                            <label>
                                <input type="checkbox" name="shared">Dieses Lesezeichen teilen                            </label>
                            <button class="btn btn-primary" type="submit" name="submit">OK</button>
                        </div> <!-- options -->
                        <div class="query_input">
                            <span class="bookmark_add_input"></span>
                        </div>
                    </div>
                </div> <!-- Add bookmark card -->
            </div> <!-- Bookmarks card -->
                        <div class="card" id="pma_console_options">
            <div class="toolbar ">
                    <div class="switch_button">
            
            <span>Optionen</span>
        </div>
                            <div class="button default">
            
            <span>Standard festlegen</span>
        </div>
            </div>
            <div class="content">
                <label>
                    <input type="checkbox" name="always_expand">Abfragenachrichten immer aufklappen                </label>
                <br>
                <label>
                    <input type="checkbox" name="start_history">Abfrageverlauf beim Start anzeigen                </label>
                <br>
                <label>
                    <input type="checkbox" name="current_query">Aktuelle Abfrage anzeigen                </label>
                <br>
                <label>
                    <input type="checkbox" name="enter_executes">
                        Execute queries on Enter and insert new line with Shift+Enter. To make this permanent, view settings.                </label>
                <br>
                <label>
                    <input type="checkbox" name="dark_theme">Zum dunklen Motiv wechseln                </label>
                <br>
            </div>
        </div> <!-- Options card -->
        <div class="templates">
                        <div class="query_actions">
                    <span class="action collapse">
            Zuklappen
                    </span>
                            <span class="action expand">
            Aufklappen
                    </span>
                            <span class="action requery">
            Erneut abfragen
                    </span>
                            <span class="action edit">
            Bearbeiten
                    </span>
                            <span class="action explain">
            Erklären
                    </span>
                            <span class="action profiling">
            Messen
                    </span>
                            <span class="action bookmark">
            Lesezeichen
                    </span>
                            <span class="text failed">
            Abfrage fehlgeschlagen
                    </span>
                            <span class="text targetdb">
            Datenbank
                            : <span></span>
                    </span>
                            <span class="text query_time">
            Abfragezeit
                            : <span></span>
                    </span>
            </div>
        </div>
    </div> <!-- #console end -->
</div> <!-- #console_container end -->


  <div id="page_content">
    

    
    <div class="modal fade" id="previewSqlModal" tabindex="-1" aria-labelledby="previewSqlModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="previewSqlModalLabel">Loading</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schließen"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

    <div class="modal fade" id="enumEditorModal" tabindex="-1" aria-labelledby="enumEditorModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="enumEditorModalLabel">ENUM/SET Editor</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schließen"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="enumEditorGoButton" data-bs-dismiss="modal">OK</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

    <div class="modal fade" id="createViewModal" tabindex="-1" aria-labelledby="createViewModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" id="createViewModalDialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="createViewModalLabel">Erzeuge View</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Schließen"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" id="createViewModalGoButton">OK</button>
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>

<div id="page_settings_modal"><div class="page_settings"><form method="post" action="index.php&#x3F;route&#x3D;&#x25;2Fdatabase&#x25;2Fstructure&amp;db&#x3D;krautundrueben&amp;server&#x3D;1" class="config-form disableAjax">
  <input type="hidden" name="tab_hash" value="">
      <input type="hidden" name="check_page_refresh" id="check_page_refresh" value="">
    <input type="hidden" name="token" value="777a4c486c5068465748377176516346">
  <input type="hidden" name="submit_save" value="DbStructure">

  <ul class="nav nav-tabs" id="configFormDisplayTab" role="tablist">
          <li class="nav-item" role="presentation">
        <a class="nav-link active" id="DbStructure-tab" href="#DbStructure" data-bs-toggle="tab" role="tab" aria-controls="DbStructure" aria-selected="true">Datenbank-Struktur</a>
      </li>
      </ul>
  <div class="tab-content">
          <div class="tab-pane fade show active" id="DbStructure" role="tabpanel" aria-labelledby="DbStructure-tab">
        <div class="card border-top-0">
          <div class="card-body">
            <h5 class="card-title visually-hidden">Datenbank-Struktur</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Wählen Sie aus, welche Details in der Datenbankstrukturansicht (Liste der Tabellen) angezeigt werden sollen.</h6>
            
            <fieldset class="optbox">
              <legend>Datenbank-Struktur</legend>

                            
              <table class="table table-borderless">
                <tr>
  <th>
    <label for="ShowDbStructureCharset">Tabellen-Zeichensatz einblenden</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDbStructureCharset" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Eine Spalte mit dem Zeichensatz für alle Tabellen ein-/ausblenden.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDbStructureCharset" id="ShowDbStructureCharset">
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDbStructureCharset" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="ShowDbStructureComment">Tabellen-Kommentare einblenden</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDbStructureComment" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Eine Spalte mit den Kommentaren für alle Tabellen ein-/ausblenden.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDbStructureComment" id="ShowDbStructureComment">
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDbStructureComment" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="ShowDbStructureCreation">Erstellungs-Zeitstempel anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDbStructureCreation" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Eine Spalte mit dem Erstellungs-Zeitstempel für alle Tabellen ein-/ausblenden.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDbStructureCreation" id="ShowDbStructureCreation">
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDbStructureCreation" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="ShowDbStructureLastUpdate">Zeitstempel des letzten Updates anzeigen</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDbStructureLastUpdate" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Eine Zeile mit dem Zeitstempel des letzten Updates für alle Tabellen ein-/ausblenden.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDbStructureLastUpdate" id="ShowDbStructureLastUpdate">
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDbStructureLastUpdate" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>
<tr>
  <th>
    <label for="ShowDbStructureLastCheck">Zeitstempel der letzten Überprüfung einblenden</label>

          <span class="doc">
        <a href="./doc/html/config.html#cfg_ShowDbStructureLastCheck" target="documentation"><img src="themes/dot.gif" title="Dokumentation" alt="Dokumentation" class="icon ic_b_help"></a>
      </span>
    
    
          <small>Eine Zeile mit dem Zeitstempel der letzten Überprüfung für alle Tabellen ein-/ausblenden.</small>
      </th>

  <td>
          <span class="checkbox">
        <input type="checkbox" name="ShowDbStructureLastCheck" id="ShowDbStructureLastCheck">
      </span>
    
    
    
          <a class="restore-default hide" href="#ShowDbStructureLastCheck" title="Voreingestellten Wert wiederherstellen"><img src="themes/dot.gif" title="Voreingestellten Wert wiederherstellen" alt="Voreingestellten Wert wiederherstellen" class="icon ic_s_reload"></a>
    
          </td>

  </tr>

              </table>
            </fieldset>
          </div>

                  </div>
      </div>
      </div>
</form>

<script type="text/javascript">
  if (typeof configInlineParams === 'undefined' || !Array.isArray(configInlineParams)) {
    configInlineParams = [];
  }
  configInlineParams.push(function () {
    ;

    $.extend(Messages, {
      'error_nan_p': 'Keine\u0020positive\u0020Zahl\u0021',
      'error_nan_nneg': 'Keine\u0020nicht\u002Dnegative\u0020Zahl\u0021',
      'error_incorrect_port': 'Keine\u0020g\u00FCltige\u0020Portnummer\u0021',
      'error_invalid_value': 'Ung\u00FCltiger\u0020Wert\u0021',
      'error_value_lte': 'Wert\u0020muss\u0020gleich\u0020oder\u0020kleiner\u0020als\u0020\u0025s\u0020sein\u0021',
    });

    $.extend(defaultValues, {
      'ShowDbStructureCharset': false,
      'ShowDbStructureComment': false,
      'ShowDbStructureCreation': false,
      'ShowDbStructureLastUpdate': false,
      'ShowDbStructureLastCheck': false
    });
  });
  if (typeof configScriptLoaded !== 'undefined' && configInlineParams) {
    loadInlineConfig();
  }
</script>
</div></div>
  <div id="tableslistcontainer">
    

    <div class="card mb-3" id="tableFilter">
  <div class="card-header">Filter</div>
  <div class="card-body row row-cols-lg-auto gy-1 gx-3 align-items-center">
    <label class="col-12 col-form-label" for="filterText">Beinhalten das Wort:</label>
    <div class="col-12">
      <input class="form-control" name="filterText" type="text" id="filterText" value="">
    </div>
  </div>
</div>
<form method="post" action="index.php?route=/database/structure" name="tablesForm" id="tablesForm">
<input type="hidden" name="db" value="krautundrueben"><input type="hidden" name="token" value="777a4c486c5068465748377176516346">
<div class="table-responsive">
<table class="table table-light table-striped table-hover table-sm w-auto data">
    <thead class="table-light">
        <tr>
            <th class="d-print-none"></th>
            <th><a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=table&sort_order=DESC" title="Sortierung" onmouseover="$(&#039;.sort_arrow&#039;).toggle();" onmouseout="$(&#039;.sort_arrow&#039;).toggle();">Tabelle <img src="themes/dot.gif" title="" alt="Aufsteigend" class="icon ic_s_asc sort_arrow"> <img src="themes/dot.gif" title="" alt="Absteigend" class="icon ic_s_desc sort_arrow hide"></a></th>
            
                                                                                            <th colspan="7" class="d-print-none">
                Aktion            </th>
                        <th>
                <a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=records&sort_order=DESC" title="Sortierung">Datensätze</a>
                <span class="pma_hint"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_help"><span class="hide">Es kann sich hierbei um Näherungswerte handeln. Klicken Sie auf die Nummer um eine exakte Anzahl zu bekommen. Bitte lesen Sie auch <a href="./doc/html/faq.html#faq3-11" target="documentation">FAQ 3.11</a>.</span></span>
            </th>
                            <th><a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=type&sort_order=ASC" title="Sortierung">Typ</a></th>
                <th><a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=collation&sort_order=ASC" title="Sortierung">Kollation</a></th>
            
                                            <th><a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=size&sort_order=DESC" title="Sortierung">Größe</a></th>
                                <th><a href="index.php?route=/database/structure&db=krautundrueben&pos=0&sort=overhead&sort_order=DESC" title="Sortierung">Überhang</a></th>
            
            
            
            
            
                    </tr>
    </thead>
    <tbody>
            <tr id="row_tbl_1" data-filter-row="ALLERGENE">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="ALLERGENE"
            id="checkbox_tbl_1">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=ALLERGENE&pos=0" title="">
            ALLERGENE
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="185102892a53b747e694dbe21848ab62_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=ALLERGENE&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="c010dcc783ae22e1721d0b3d3b0f0e13">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=ALLERGENE&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=ALLERGENE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=ALLERGENE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=ALLERGENE"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=ALLERGENE&sql_query=TRUNCATE+%60ALLERGENE%60&message_to_show=Die%2BTabelle%2BALLERGENE%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=ALLERGENE&reload=1&purge=1&sql_query=DROP+TABLE+%60ALLERGENE%60&message_to_show=Die+Tabelle+ALLERGENE+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="ALLERGENE">
                            3
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=ALLERGENE#showusage">
                    <span>16,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_2" data-filter-row="BESTELLUNG">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="BESTELLUNG"
            id="checkbox_tbl_2">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=BESTELLUNG&pos=0" title="">
            BESTELLUNG
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="48424e0968c39c6875dfa38561e8742a_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=BESTELLUNG&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="d608d56078a67da9dff9ee51c06b4f19">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=BESTELLUNG&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=BESTELLUNG">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=BESTELLUNG">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=BESTELLUNG"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=BESTELLUNG&sql_query=TRUNCATE+%60BESTELLUNG%60&message_to_show=Die%2BTabelle%2BBESTELLUNG%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=BESTELLUNG&reload=1&purge=1&sql_query=DROP+TABLE+%60BESTELLUNG%60&message_to_show=Die+Tabelle+BESTELLUNG+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="BESTELLUNG">
                            12
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=BESTELLUNG#showusage">
                    <span>32,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_3" data-filter-row="BESTELLUNGZUTAT">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="BESTELLUNGZUTAT"
            id="checkbox_tbl_3">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=BESTELLUNGZUTAT&pos=0" title="">
            BESTELLUNGZUTAT
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="59fe860ef5c3a670ee49e902e3e2482d_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=BESTELLUNGZUTAT&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="7d157769b6870caaa68ccc6604aac3be">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=BESTELLUNGZUTAT&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=BESTELLUNGZUTAT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=BESTELLUNGZUTAT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=BESTELLUNGZUTAT"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=BESTELLUNGZUTAT&sql_query=TRUNCATE+%60BESTELLUNGZUTAT%60&message_to_show=Die%2BTabelle%2BBESTELLUNGZUTAT%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=BESTELLUNGZUTAT&reload=1&purge=1&sql_query=DROP+TABLE+%60BESTELLUNGZUTAT%60&message_to_show=Die+Tabelle+BESTELLUNGZUTAT+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="BESTELLUNGZUTAT">
                            26
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=BESTELLUNGZUTAT#showusage">
                    <span>32,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_4" data-filter-row="BOX">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="BOX"
            id="checkbox_tbl_4">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=BOX&pos=0" title="">
            BOX
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="e657cce1913c857166b0475f18668ef5_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=BOX&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="0f51a5e6761e4336cbcc07077e497430">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=BOX&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_bd_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=BOX">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=BOX">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_bd_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=BOX"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=BOX&sql_query=TRUNCATE+%60BOX%60&message_to_show=Die%2BTabelle%2BBOX%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_bd_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=BOX&reload=1&purge=1&sql_query=DROP+TABLE+%60BOX%60&message_to_show=Die+Tabelle+BOX+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="BOX">
                            0
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=BOX#showusage">
                    <span>16,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_5" data-filter-row="KATEGORIE">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="KATEGORIE"
            id="checkbox_tbl_5">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=KATEGORIE&pos=0" title="">
            KATEGORIE
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="368e8032076c62a3323a6ec1f286c053_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=KATEGORIE&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="14de6f60277e4d762967d003b2e36611">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=KATEGORIE&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=KATEGORIE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=KATEGORIE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=KATEGORIE"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=KATEGORIE&sql_query=TRUNCATE+%60KATEGORIE%60&message_to_show=Die%2BTabelle%2BKATEGORIE%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=KATEGORIE&reload=1&purge=1&sql_query=DROP+TABLE+%60KATEGORIE%60&message_to_show=Die+Tabelle+KATEGORIE+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="KATEGORIE">
                            3
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=KATEGORIE#showusage">
                    <span>16,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_6" data-filter-row="KUNDE">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="KUNDE"
            id="checkbox_tbl_6">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=KUNDE&pos=0" title="">
            KUNDE
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="004f8a08f9ec8c86a5868225336df585_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=KUNDE&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="2dde731ff943ef1c62dad26a399686ed">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=KUNDE&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=KUNDE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=KUNDE">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=KUNDE"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=KUNDE&sql_query=TRUNCATE+%60KUNDE%60&message_to_show=Die%2BTabelle%2BKUNDE%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=KUNDE&reload=1&purge=1&sql_query=DROP+TABLE+%60KUNDE%60&message_to_show=Die+Tabelle+KUNDE+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="KUNDE">
                            9
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=KUNDE#showusage">
                    <span>16,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_7" data-filter-row="LIEFERANT">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="LIEFERANT"
            id="checkbox_tbl_7">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=LIEFERANT&pos=0" title="">
            LIEFERANT
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="29ca88444404a3ea82dba783b6da8ce6_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=LIEFERANT&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="dc749dd224e78f12edc7232034f3526e">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=LIEFERANT&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=LIEFERANT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=LIEFERANT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=LIEFERANT"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=LIEFERANT&sql_query=TRUNCATE+%60LIEFERANT%60&message_to_show=Die%2BTabelle%2BLIEFERANT%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=LIEFERANT&reload=1&purge=1&sql_query=DROP+TABLE+%60LIEFERANT%60&message_to_show=Die+Tabelle+LIEFERANT+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="LIEFERANT">
                            3
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=LIEFERANT#showusage">
                    <span>16,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
            <tr id="row_tbl_8" data-filter-row="ZUTAT">
    <td class="text-center d-print-none">
        <input type="checkbox"
            name="selected_tbl[]"
            class="checkall"
            value="ZUTAT"
            id="checkbox_tbl_8">
    </td>
    <th>
        <a href="index.php?route=/sql&db=krautundrueben&table=ZUTAT&pos=0" title="">
            ZUTAT
        </a>
        
    </th>
    
                <td class="text-center d-print-none">
                                    <a id="2402b7e3d605c01ab4c1aafd3de1ce1a_favorite_anchor"
    class="ajax favorite_table_anchor"
    href="index.php?route=/database/structure/favorite-table&db=krautundrueben&ajax_request=1&favorite_table=ZUTAT&add_favorite=1"
    title="Zu den Favoriten hinzufügen"
    data-favtargets="d3ff309e0fa791e21274e5dda5f7fee6">
    <span class="text-nowrap"><img src="themes/dot.gif" title="" alt="" class="icon ic_b_no_favorite">&nbsp;</span>
</a>
        </td>
    
    <td class="text-center d-print-none">
        <a href="index.php?route=/sql&db=krautundrueben&table=ZUTAT&pos=0">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Anzeigen" alt="Anzeigen" class="icon ic_b_browse">&nbsp;Anzeigen</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/structure&db=krautundrueben&table=ZUTAT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Struktur" alt="Struktur" class="icon ic_b_props">&nbsp;Struktur</span>
        </a>
    </td>
    <td class="text-center d-print-none">
        <a href="index.php?route=/table/search&db=krautundrueben&table=ZUTAT">
          <span class="text-nowrap"><img src="themes/dot.gif" title="Suche" alt="Suche" class="icon ic_b_select">&nbsp;Suche</span>
        </a>
    </td>

            <td class="insert_table text-center d-print-none">
            <a href="index.php?route=/table/change&db=krautundrueben&table=ZUTAT"><span class="text-nowrap"><img src="themes/dot.gif" title="Einfügen" alt="Einfügen" class="icon ic_b_insrow">&nbsp;Einfügen</span></a>
        </td>
                  <td class="text-center d-print-none">
                <a class="truncate_table_anchor ajax" href="index.php?route=/sql" data-post="db=krautundrueben&table=ZUTAT&sql_query=TRUNCATE+%60ZUTAT%60&message_to_show=Die%2BTabelle%2BZUTAT%2Bwurde%2Bgeleert.">
                  <span class="text-nowrap"><img src="themes/dot.gif" title="Leeren" alt="Leeren" class="icon ic_b_empty">&nbsp;Leeren</span>
                </a>
          </td>
                <td class="text-center d-print-none">
            <a class="ajax drop_table_anchor" href="index.php?route=/sql" data-post="db=krautundrueben&table=ZUTAT&reload=1&purge=1&sql_query=DROP+TABLE+%60ZUTAT%60&message_to_show=Die+Tabelle+ZUTAT+wurde+gel%C3%B6scht.">
                <span class="text-nowrap"><img src="themes/dot.gif" title="Löschen" alt="Löschen" class="icon ic_b_drop">&nbsp;Löschen</span>
            </a>
        </td>
    
                    
                <td class="value tbl_rows font-monospace text-end"
            data-table="ZUTAT">
                            22
                        
        </td>

                    <td class="text-nowrap">
                                    InnoDB
                            </td>
                            <td class="text-nowrap">
                    <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung">utf8mb4_general_ci</dfn>

                </td>
                    
                    <td class="value tbl_size font-monospace text-end">
                <a href="index.php?route=/table/structure&db=krautundrueben&table=ZUTAT#showusage">
                    <span>32,0</span>&nbsp;<span class="unit">KiB</span>
                </a>
            </td>
            <td class="value tbl_overhead font-monospace text-end">
                -
            </td>
        
                            
        
        
        
        
    </tr>
        </tbody>
            <tfoot id="tbl_summary_row" class="table-light">
<tr>
    <th class="d-print-none"></th>
    <th class="tbl_num text-nowrap">
                8 Tabellen
    </th>
                <th colspan="7" class="d-print-none">Gesamt</th>
                                                <th class="value tbl_rows font-monospace text-end">78</th>
                                    <th class="text-center">
            <dfn title="InnoDB ist die Standard Storage-Engine dieses MySQL-Servers.">
                InnoDB
            </dfn>
        </th>
        <th>
                            <dfn title="Unicode (UCA 4.0.0), Beachtet nicht Groß- und Kleinschreibung (Standard)">
                    utf8mb4_general_ci
                </dfn>
                    </th>
    
                                    <th class="value tbl_size font-monospace text-end">176,0 KiB</th>

                                <th class="value tbl_overhead font-monospace text-end">0 B</th>
    
                    </tr>
</tfoot>
    </table>
</div>
    <div class="clearfloat d-print-none">
    <img class="selectallarrow" src="./themes/pmahomme/img/arrow_ltr.png" width="38" height="22" alt="markierte:">
    <input type="checkbox" id="tablesForm_checkall" class="checkall_box" title="Alle auswählen">
    <label for="tablesForm_checkall">Alle auswählen</label>
        <select name="submit_mult" style="margin: 0 3em 0 3em;">
        <option value="markierte:" selected="selected">markierte:</option>
        <option value="copy_tbl">Tabelle kopieren</option>
        <option value="show_create">Erzeugung anzeigen</option>
        <option value="export">Exportieren</option>
                    <optgroup label="Daten oder Tabelle löschen">
                <option value="empty_tbl">Leeren</option>
                <option value="drop_tbl">Löschen</option>
            </optgroup>
            <optgroup label="Hilfsmittel">
                <option value="analyze_tbl">Analysiere Tabelle</option>
                <option value="check_tbl">Überprüfe Tabelle</option>
                <option value="checksum_tbl">Prüfsummentabelle</option>
                <option value="optimize_tbl">Optimiere Tabelle</option>
                <option value="repair_tbl">Repariere Tabelle</option>
            </optgroup>
            <optgroup label="Präfix">
                <option value="add_prefix_tbl">Prefix der Tabelle voranstellen</option>
                <option value="replace_prefix_tbl">Tabellenprefix ersetzen</option>
                <option value="copy_tbl_change_prefix">Tabelle mit Prefix kopieren</option>
            </optgroup>
                            <optgroup label="Zentrale Spalten">
                <option value="sync_unique_columns_central_list">Spalten zur zentralen Liste hinzufügen</option>
                <option value="delete_unique_columns_central_list">Entfernen von Spalten aus der zentralen Liste</option>
                <option value="make_consistent_with_central_list">Konsistent mit zentraler Liste machen</option>
            </optgroup>
            </select>
    
</div>

<div class="modal fade" id="bulkActionModal" data-bs-backdrop="static" data-bs-keyboard="false"
     tabindex="-1" aria-labelledby="bulkActionLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bulkActionLabel"></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Abbrechen"></button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Abbrechen</button>
        <button type="button" class="btn btn-primary" id="bulkActionContinue">Weiter</button>
      </div>
    </div>
  </div>
</div>

  <div class="modal fade" id="makeConsistentWithCentralListModal" data-bs-backdrop="static" data-bs-keyboard="false"
       tabindex="-1" aria-labelledby="makeConsistentWithCentralListModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="makeConsistentWithCentralListModalLabel">Sind sie sicher?</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Abbrechen"></button>
        </div>
        <div class="modal-body">
          Diese Aktion kann einige Spalten-Definitionen ändern.<br>Sind Sie sicher, dass Sie fortfahren möchten?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Abbrechen</button>
          <button type="button" class="btn btn-primary" id="makeConsistentWithCentralListContinue">Weiter</button>
        </div>
      </div>
    </div>
  </div>
</form>


    
  </div>
  <hr>
  <p class="d-print-none">
    <button type="button" class="btn btn-link p-0 jsPrintButton"><span class="text-nowrap"><img src="themes/dot.gif" title="Drucken" alt="Drucken" class="icon ic_b_print">&nbsp;Drucken</span></button>
    <a href="index.php?route=/database/data-dictionary&db=krautundrueben&goto=index.php%3Froute%3D%2Fdatabase%2Fstructure">
      <span class="text-nowrap"><img src="themes/dot.gif" title="Strukturverzeichnis" alt="Strukturverzeichnis" class="icon ic_b_tblanalyse">&nbsp;Strukturverzeichnis</span>
    </a>
  </p>

  <form id="createTableMinimalForm" method="post" action="index.php?route=/table/create" class="card d-print-none lock-page">
  <input type="hidden" name="db" value="krautundrueben"><input type="hidden" name="token" value="777a4c486c5068465748377176516346">
  <div class="card-header"><span class="text-nowrap"><img src="themes/dot.gif" title="Create new table" alt="Create new table" class="icon ic_b_table_add">&nbsp;Create new table</span></div>
  <div class="card-body row row-cols-lg-auto g-3">
    <div class="col-12">
      <label for="createTableNameInput" class="form-label">Tabellenname</label>
      <input type="text" class="form-control" name="table" id="createTableNameInput" maxlength="64" required>
    </div>
    <div class="col-12">
      <label for="createTableNumFieldsInput" class="form-label">Anzahl der Spalten</label>
      <input type="number" class="form-control" name="num_fields" id="createTableNumFieldsInput" min="1" value="4" required>
    </div>
    <div class="col-12 align-self-lg-end">
      <input class="btn btn-primary" type="submit" value="Anlegen">
    </div>
  </div>
</form>

  </div>
      <div id="selflink" class="d-print-none">
      <a href="index.php?route=%2Fdatabase%2Fstructure&amp;db=krautundrueben&amp;server=1" title="Neues phpMyAdmin-Fenster" target="_blank" rel="noopener noreferrer">
                  <img src="themes/dot.gif" title="Neues phpMyAdmin-Fenster" alt="Neues phpMyAdmin-Fenster" class="icon ic_window-new">
              </a>
    </div>
  
  <div class="clearfloat d-print-none" id="pma_errors">
    
  </div>

  
<script data-cfasync="false" type="text/javascript">
// <![CDATA[
var debugSQLInfo = 'null';

// ]]>
</script>


  
  
  </body>
</html>
